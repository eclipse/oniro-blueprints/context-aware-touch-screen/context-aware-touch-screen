/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 */

#include <stdlib.h>

#include "cats.h"

static const lv_coord_t col_dsc[] = {
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_TEMPLATE_LAST
};

static const lv_coord_t row_dsc[] = {
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_TEMPLATE_LAST
};

static struct cats_screen scr;
static lv_obj_t *meter;

static void meter_set_value(void *indic, int32_t v)
{
	lv_meter_set_indicator_value(meter, indic, v);
}

static void create_meter(void)
{
	lv_meter_indicator_t *indic;
	lv_meter_scale_t *scale;
	lv_anim_t anim;

	meter = lv_meter_create(scr.obj);
	lv_obj_set_size(meter, 130, 120);
	lv_obj_align(meter, LV_ALIGN_BOTTOM_MID, 0, 0);

	scale = lv_meter_add_scale(meter);
	lv_meter_set_scale_ticks(meter, scale, 41, 2, 10,
				 lv_palette_main(LV_PALETTE_GREY));
	lv_meter_set_scale_major_ticks(meter, scale, 8, 4, 15,
				       lv_color_black(), 10);

	indic = lv_meter_add_arc(meter, scale, 3,
				 lv_palette_main(LV_PALETTE_BLUE), 0);
	lv_meter_set_indicator_start_value(meter, indic, 0);
	lv_meter_set_indicator_end_value(meter, indic, 20);

	indic = lv_meter_add_scale_lines(meter, scale,
					 lv_palette_main(LV_PALETTE_BLUE),
					 lv_palette_main(LV_PALETTE_BLUE),
					 false, 0);
	lv_meter_set_indicator_start_value(meter, indic, 0);
	lv_meter_set_indicator_end_value(meter, indic, 20);

	indic = lv_meter_add_arc(meter, scale, 3,
				 lv_palette_main(LV_PALETTE_RED), 0);
	lv_meter_set_indicator_start_value(meter, indic, 80);
	lv_meter_set_indicator_end_value(meter, indic, 100);

	indic = lv_meter_add_scale_lines(meter, scale,
					 lv_palette_main(LV_PALETTE_RED),
					 lv_palette_main(LV_PALETTE_RED),
					 false, 0);
	lv_meter_set_indicator_start_value(meter, indic, 80);
	lv_meter_set_indicator_end_value(meter, indic, 100);

	indic = lv_meter_add_needle_line(meter, scale, 4,
					 lv_palette_main(LV_PALETTE_GREY), -10);

	lv_anim_init(&anim);
	lv_anim_set_exec_cb(&anim, meter_set_value);
	lv_anim_set_var(&anim, indic);
	lv_anim_set_values(&anim, 0, 100);
	lv_anim_set_time(&anim, 2000);
	lv_anim_set_repeat_delay(&anim, 100);
	lv_anim_set_playback_time(&anim, 500);
	lv_anim_set_playback_delay(&anim, 100);
	lv_anim_set_repeat_count(&anim, LV_ANIM_REPEAT_INFINITE);
	lv_anim_start(&anim);
}

void register_demo_screen()
{
	lv_obj_t *btn, *keypad, *label, *cb, *slider;
	int i, j;

	LOG_DBG("Creating demo screen");

	scr.name = "demo";
	scr.obj = lv_obj_create(NULL);
	scr.orientation = CATS_SCREEN_HORIZONTAL;

	keypad = lv_obj_create(scr.obj);
	lv_obj_set_style_grid_column_dsc_array(keypad, col_dsc, 0);
	lv_obj_set_style_grid_row_dsc_array(keypad, row_dsc, 0);
	lv_obj_set_size(keypad, 240, 140);
	lv_obj_center(keypad);
	lv_obj_set_layout(keypad, LV_LAYOUT_GRID);
	lv_obj_align(keypad, LV_ALIGN_TOP_MID, 0, 0);

	for (i = 0; i < 3; i++) {
		for (j = 0; j < 2; j++) {
			btn = lv_btn_create(keypad);
			lv_obj_set_grid_cell(btn, LV_GRID_ALIGN_STRETCH, i, 1,
						 LV_GRID_ALIGN_STRETCH, j, 1);

			label = lv_label_create(btn);
			lv_label_set_text_fmt(label, "BTN %d.%d", i, j);
			lv_obj_center(label);
		}
	}

	cb = lv_checkbox_create(scr.obj);
	lv_checkbox_set_text(cb, "Hello Oniro!");
	lv_obj_align(cb, LV_ALIGN_CENTER, 0, 0);

	slider = lv_slider_create(scr.obj);
	lv_obj_set_width(slider, 180);
	lv_obj_align_to(slider, cb, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);

	create_meter();

	cats_add_screen(&scr);
}
