// SPDX-FileCopyrightText: Huawei Inc.
// SPDX-License-Identifier: Apache-2.0

#define _DEFAULT_SOURCE /* needed for usleep() */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cats.h"
#include "screens.h"

static void hal_init(void); /* see {sdl,...}.c */

static struct cats_screen main_scr;
static struct cats_screen *active_screen = &main_scr;
static struct cats_screen *tail_screen = &main_scr;

LV_IMG_DECLARE(oniro_icon);

static void load_screen(struct cats_screen *scr)
{
	if (scr == NULL) return; 
	LOG_DBG("Loading screen: %s", scr->name);

	lv_scr_load(scr->obj);
	active_screen = scr;
}

static void gesture_event_cb(lv_event_t *event)
{
	struct cats_screen *scr;

	lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());

	switch (dir) {
	case LV_DIR_LEFT:
		LOG_DBG("Left swipe");
		if (active_screen->orientation == CATS_SCREEN_VERTICAL)
			break;

		load_screen(active_screen->next);
		break;
	case LV_DIR_RIGHT:
		LOG_DBG("Right swipe");
		if (active_screen->orientation == CATS_SCREEN_VERTICAL)
			break;

		load_screen(active_screen->prev);
		break;
	case LV_DIR_TOP:
		LOG_DBG("Top swipe");
		if (active_screen->orientation == CATS_SCREEN_HORIZONTAL)
			break;

		load_screen(active_screen->next);
		break;
	case LV_DIR_BOTTOM:
		LOG_DBG("Bottom swipe");
		if (active_screen->orientation == CATS_SCREEN_HORIZONTAL)
			break;

		load_screen(active_screen->prev);
		break;
	default:
		break;
	}
}

static void init_gesture(lv_obj_t *obj)
{
	lv_obj_add_event_cb(obj, gesture_event_cb, LV_EVENT_GESTURE, NULL);
}

void cats_add_screen(struct cats_screen *scr)
{
	init_gesture(scr->obj);
	tail_screen->next = scr;
	scr->prev = tail_screen;
	tail_screen = scr;
}

static void start_main_screen(void)
{
	lv_obj_t *label, *img;

	LOG_DBG("Creating main screen");

	main_scr.obj = lv_obj_create(NULL);
	main_scr.name = "main screen";

	label = lv_label_create(main_scr.obj);
	lv_label_set_text(label, "Oniro CATS");
	lv_obj_align(label, LV_ALIGN_TOP_MID, 0, 30);

	init_gesture(main_scr.obj);

	img = lv_img_create(main_scr.obj);
	lv_img_set_src(img, &oniro_icon);
	lv_obj_align(img, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_size(img, 230, 230);

	load_screen(&main_scr);
}

int main(void)
{
	LOG_INFO("Context-Aware Touch Screen app ready");

	lv_init();
	hal_init();

	register_keypad_screen();
	register_demo_screen();
	register_settings_screen();
	register_developer_screen();

	start_main_screen();

	for (;;) {
		lv_timer_handler();
		usleep(5 * 1000);
	}

	return 0;
}
