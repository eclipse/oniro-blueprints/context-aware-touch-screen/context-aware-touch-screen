/*
 * SPDX-License-Identifier: Apache-2.0

 * SPDX-FileCopyrightText: Huawei Inc.
 */

#ifndef _DEMO_H_
#define _DEMO_H_

void register_demo_screen();
void register_developer_screen();
void register_keypad_screen();
void register_settings_screen();

#endif /* _DEMO_H_ */
